const express = require("express");
const OAuth = require("oauth-1.0a");
const crypto = require("crypto");
const fetch = require("node-fetch");
const cors = require("cors");
const http = require("http");
const dotenv = require("dotenv");

dotenv.config();

const app = express();

const corsOptions = {
  origin: function (origin, callback) {
    if (origin === process.env.REACT_APP_CLIENT_URL) {
      callback(null, true)
    } else {
      callback(new Error('Not allowed by CORS'))
    }
  }
}

const oauth = new OAuth({
  consumer: { key: process.env.REACT_APP_FLICKR_API_KEY, secret: process.env.REACT_APP_FLICKR_API_SECRET },
  signature_method: 'HMAC-SHA1',
  hash_function(base_string, key) {
    return crypto.createHmac('sha1', key).update(base_string).digest('base64');
  },
});

app.get("/request_token", cors(corsOptions), async (req, res) => {
  const oauth_callback = process.env.NODE_ENV === "dev" ? process.env.REACT_APP_CLIENT_URL : req.headers.origin;

  const requestTokenDataOptions = {
    url: "https://api.flickr.com/services/oauth/request_token",
    method: "GET",
    data: {
      oauth_callback,
    }
  };
  const authorizationData = oauth.authorize(requestTokenDataOptions);
  const params = oauth.getParameterString(requestTokenDataOptions, authorizationData);
  const url = `${requestTokenDataOptions.url}?${params}`;

  const { oauthToken, oauthTokenSecret } = await fetch(url).then(res => res.text()).then((data) => {
    const dataSearch = new URLSearchParams(data);
    const oauthToken = dataSearch.get("oauth_token");
    const oauthTokenSecret = dataSearch.get("oauth_token_secret");

    return { oauthToken, oauthTokenSecret };
  });

  res.send({ oauthToken, oauthTokenSecret });
});

app.get("/access_token", cors(corsOptions), async (req, res) => {
  const { oauthToken, oauthTokenSecret, oauthVerifier } = req.query;
  const token = {
    key: oauthToken,
    secret: oauthTokenSecret
  };
  const requestTokenDataOptions = {
    url: "https://api.flickr.com/services/oauth/access_token",
    method: "GET",
    data: {
      oauth_verifier: oauthVerifier
    }
  };
  const authorizationData = oauth.authorize(requestTokenDataOptions, token);
  const params = oauth.getParameterString(requestTokenDataOptions, authorizationData);
  const url = `${requestTokenDataOptions.url}?${params}`;

  const userNsid = await fetch(url).then(res => res.text()).then(data => {
    const dataSearch = new URLSearchParams(data);

    return dataSearch.get("user_nsid");
  });

  res.send({ userNsid });
});

app.get("/add_favourite_image", cors(corsOptions), async (req, res) => {
  const { oauthToken, oauthTokenSecret, oauthVerifier, photoId } = req.query;
  const requestTokenDataOptions = {
    url: "https://api.flickr.com/services/rest",
    method: "POST",
    data: {
      method: "flickr.favorites.add",
      format: "json",
      nojsoncallback: 1,
      photo_id: photoId,
      oauth_verifier: oauthVerifier,
    }
  };
  const token = {
    key: oauthToken,
    secret: oauthTokenSecret
  };

  const authorizationData = oauth.authorize(requestTokenDataOptions, token);
  const params = oauth.getParameterString(requestTokenDataOptions, authorizationData);
  const url = `${requestTokenDataOptions.url}?${params}`;

  fetch(url, { method: requestTokenDataOptions.method })
    .then(response => response.json())
    .then(data => {
      if (data.code === 3) {
        res.send({
          ...data,
          message: "Photo is already in favourites"
        });
        return;
      }

      res.send(data);
    });
});

app.get("/remove_favourite_image", cors(corsOptions), async (req, res) => {
  const { oauthToken, oauthTokenSecret, oauthVerifier, photoId } = req.query;
  const requestTokenDataOptions = {
    url: "https://api.flickr.com/services/rest",
    method: "POST",
    data: {
      method: "flickr.favorites.remove",
      format: "json",
      nojsoncallback: 1,
      photo_id: photoId,
      oauth_verifier: oauthVerifier,
    }
  };
  const token = {
    key: oauthToken,
    secret: oauthTokenSecret
  };

  const authorizationData = oauth.authorize(requestTokenDataOptions, token);
  const params = oauth.getParameterString(requestTokenDataOptions, authorizationData);
  const url = `${requestTokenDataOptions.url}?${params}`;

  fetch(url, { method: requestTokenDataOptions.method })
    .then(response => response.json())
    .then(data => res.send(data));
});

http.createServer(app).listen(process.env.HTTP_PORT, () => {
  console.log(`Express HTTP server now running on PORT:${process.env.HTTP_PORT}`);
});
