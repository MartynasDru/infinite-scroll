import React, { useEffect } from "react";
import {
  Switch,
  Route,
  useLocation,
} from "react-router-dom";
import RecentImages from "pages/RecentImages/RecentImages";
import Favourites from "pages/Favourites/Favourites";
import routes from "app/routes";
import Navigation from "components/Navigation/Navigation";
import { authenticateUser } from "services/flickrAuthenticationService";

const App = () => {
  const { search } = useLocation();

  useEffect(() => {
    authenticateUser(search);
  }, []);

  return (
    <div className="App">
      <Navigation />
      <Switch>
        <Route path={routes.favourites.path}>
          <Favourites />
        </Route>
        <Route path={routes.home.path}>
          <RecentImages />
        </Route>
      </Switch>
    </div>
  );
};

export default App;
