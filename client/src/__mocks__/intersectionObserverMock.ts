const intersectionObserverMock = () => ({
  observe: jest.fn(),
});

window.IntersectionObserver = jest.fn().mockImplementation(intersectionObserverMock);
