const windowLocationMock = () => {
  const locationReplaceMock = jest.fn();
  const windowLocation = { ...window.location };
  delete window.location;
  window.location = { ...windowLocation, replace: locationReplaceMock };

  return locationReplaceMock;
};

export default windowLocationMock;
