export interface route {
  path: string;
  title: string;
}

const routes: { [key: string]: route } = {
  home: {
    path: "/",
    title: "Home",
  },
  favourites: {
    path: "/favourites",
    title: "Favourites",
  },
};

export default routes;
