import React from "react";
import { joinTruthy } from "utils/utils";
import styles from "./button.scss";

export interface ButtonProps extends React.DetailedHTMLProps<
  React.ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement
> {
  className?: string;
}

export const Button = ({ onClick, children, className }: ButtonProps) => (
  <button
    data-testid="button"
    className={
      joinTruthy([
        styles.button,
        className,
      ])
    }
    onClick={onClick}
    type="submit"
  >
    {children}
  </button>
);
