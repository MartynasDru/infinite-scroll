import React from "react";
import Enzyme from "enzyme";
import { Button, ButtonProps } from "../Button";

describe("Button", () => {
  const setup = () => {
    const props: ButtonProps = { onClick: jest.fn() };

    const wrapper = Enzyme.mount(<Button {...props} />);

    return {
      wrapper,
      props,
    };
  };

  it("renders without crashing", () => {
    setup();
  });

  it("should execute onClick when button is clicked", () => {
    const { wrapper, props } = setup();
    wrapper.find("[data-testid='button']").simulate("click");
    expect(props.onClick).toBeCalled();
  });
});
