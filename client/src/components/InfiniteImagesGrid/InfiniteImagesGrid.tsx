import React, {
  Dispatch, SetStateAction, useCallback, useEffect, useRef, useState,
} from "react";
import { ImageCard } from "components/InfiniteImagesGrid/components/ImageCard/ImageCard";
import { Loader } from "components/Loader/Loader";
import { joinTruthy } from "utils/utils";
import { ImagesData } from "services/useImageServices";
import styles from "./infinite-images-grid.scss";

export interface InfiniteImagesGridProps {
  images: ImagesData | null;
  setImages?: Dispatch<SetStateAction<ImagesData>>;
  onFetchData: (page: number) => Promise<number>;
  emptyMessage?: string;
  isFavouritesTable?: boolean;
}

export const InfiniteImagesGrid = ({
  images, setImages, onFetchData, emptyMessage, isFavouritesTable,
}: InfiniteImagesGridProps) => {
  const loaderRef = useRef(null);
  const [page, setPage] = useState<number>(0);
  const [isLoaderVisible, setIsLoaderVisible] = useState(true);

  const handleObserver = useCallback((entries) => {
    const target = entries[0];

    if (target.isIntersecting) {
      setPage((prevPage) => prevPage + 1);
    }
  }, []);

  useEffect(() => {
    const options = {
      root: null,
      rootMargin: "20px",
      threshold: 0,
    } as object;
    const observer = new IntersectionObserver(handleObserver, options);
    if (loaderRef.current) {
      observer.observe(loaderRef.current);
    }
  }, [handleObserver]);

  useEffect(() => {
    if (page > 0) {
      onFetchData(page).then((pages) => {
        setIsLoaderVisible(page !== pages && pages > 0);
      });
    }
  }, [page]);

  if (images && images.length === 0) {
    return <div data-testid="empty-message" className={styles["empty-message"]}>{emptyMessage}</div>;
  }

  const handleImagesUpdate = (id: string) => {
    const updatedImages = images.filter((image) => image.id !== id);
    setImages(updatedImages);
  };

  return (
    <div className={styles["infinite-images-grid"]}>
      {images?.map((image) => (
        <ImageCard
          key={image.id}
          data={image}
          setImages={handleImagesUpdate}
          isInFavourites={isFavouritesTable}
        />
      ))}
      {isLoaderVisible && (
        <Loader
          ref={loaderRef}
          classNames={{
            loader: joinTruthy(
              [
                styles["infinite-images-grid__loader"],
                (!images || images.length === 0) && styles["infinite-images-grid__loader--center"],
              ],
            ),
            spinner: joinTruthy(
              [
                images?.length > 0 && styles["infinite-images-grid__loader-spinner--bottom"],
              ],
            ),
          }}
        />
      )}
    </div>
  );
};
