import React from "react";
import Enzyme from "enzyme";
import { InfiniteImagesGridProps, InfiniteImagesGrid } from "../InfiniteImagesGrid";
import "__mocks__/intersectionObserverMock";

describe("InfiniteImagesGrid", () => {
  const setup = (_props?: Partial<InfiniteImagesGridProps>) => {
    const props: InfiniteImagesGridProps = {
      images: [],
      onFetchData: () => Promise.resolve(1),
      emptyMessage: "",
      ..._props,
    };

    const wrapper = Enzyme.mount(<InfiniteImagesGrid {...props} />);

    return {
      wrapper,
      props,
    };
  };

  const images = [
    {
      server: "",
      id: "id-1",
      secret: "",
      ownername: "",
      title: "",
    },
    {
      server: "",
      id: "id-2",
      secret: "",
      ownername: "",
      title: "",
    },
  ];

  it("renders without crashing", () => {
    setup();
  });

  it("should return empty message if images property is empty array", () => {
    const emptyMessage = "EMPTY MESSAGE";
    const { wrapper } = setup({ emptyMessage });
    expect(wrapper.find("[data-testid='empty-message']").text()).toBe(emptyMessage);
  });

  it("should render as many image cards as there is passed through props", () => {
    const { wrapper } = setup({ images });
    expect(wrapper.find("[data-testid='image-card']").length).toBe(images.length);
  });

  it("should show loader if there is images and onFetchData return more than 0 pages and current page is not equal to pages that exist", () => {
    const { wrapper } = setup({ images, onFetchData: () => Promise.resolve(10) });
    expect(wrapper.find("[data-testid='loader']").length).toBe(1);
  });
});
