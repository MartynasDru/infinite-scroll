import React, {
  useCallback, useEffect, useRef, useState,
} from "react";
import { Button } from "components/Button/Button";
import { ImageData, useAddRemoveFavouriteImage } from "services/useImageServices";
import { Loader } from "components/Loader/Loader";
import getImageClass from "./utils/utils";
import styles from "./image-card.scss";

export interface ImageCardProps {
  data: ImageData;
  isInFavourites?: boolean;
  setImages?: (id: string) => void;
}

export const ImageCard = ({ data, setImages, isInFavourites }: ImageCardProps) => {
  const imageCardRef = useRef(null);
  const {
    server, id, secret, ownername, title,
  } = data;
  const {
    isLoading,
    isSuccess,
    handleAddFavouriteImage,
    handleRemoveFavouriteImage,
    message,
  } = useAddRemoveFavouriteImage();
  const [isVisible, setIsVisible] = useState<boolean>(false);
  const [imageClass, setImageClass] = useState<string>(null);

  const handleFavouriteButtonClick = () => {
    if (isInFavourites) {
      handleRemoveFavouriteImage(id).then(() => {
        setImages(id);
      });
    } else {
      handleAddFavouriteImage(id);
    }
  };

  const handleObserver = useCallback((entries) => {
    const target = entries[0];

    if (target.isIntersecting) {
      setIsVisible(true);
    }
  }, []);

  useEffect(() => {
    const options = {
      root: null,
      rootMargin: "20px",
      threshold: 0,
    } as object;
    const observer = new IntersectionObserver(handleObserver, options);
    if (imageCardRef.current) {
      observer.observe(imageCardRef.current);
    }
  }, [handleObserver]);

  useEffect(() => {
    const updatedImageClass = getImageClass(imageCardRef.current.offsetWidth);
    setImageClass(updatedImageClass);
  }, []);

  return (
    <div
      ref={imageCardRef}
      data-testid="image-card"
      className={styles["image-card"]}
      tabIndex={0}
      role="gridcell"
    >
      {isVisible && imageClass && (
        <img
          alt={title}
          src={
            `https://live.staticflickr.com/${server}/${id}_${secret}${imageClass === "none" ? "" : `_${imageClass}`}.jpg`
          }
        />
      )}
      <div className={styles["image-card__hover-text"]}>
        <div className={styles["image-card__title"]}>
          <div className={styles["image-card__title-text"]}>{title}</div>
        </div>
        <div className={styles["image-card__owner-name"]}><i>{ownername}</i></div>
        <div className={styles["image-card__button-loader-wrapper"]}>
          {isLoading
            ? <Loader data-testid="add-favourite-loader" classNames={{ loader: styles["image-card__favourite-loader"] }} isLight />
            : (
              <>
                {isSuccess
                  ? (
                    <div data-testid="add-favourite-success" className={styles["image-card__favourite-success-text"]}>
                      {message || "Added to your favourites!"}
                    </div>
                  )
                  : (
                    <Button
                      data-testid="add-favourite-button"
                      className={styles["image-card__favourite-button"]}
                      onClick={handleFavouriteButtonClick}
                    >
                      {isInFavourites ? "Unfavourite" : "Favourite"}
                    </Button>
                  )}
              </>
            )}
        </div>
      </div>
    </div>
  );
};
