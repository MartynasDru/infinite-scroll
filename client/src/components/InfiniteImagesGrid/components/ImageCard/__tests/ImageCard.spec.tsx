import React from "react";
import Enzyme from "enzyme";
import * as useImageServices from "services/useImageServices";
import { ImageCard, ImageCardProps } from "../ImageCard";
import "__mocks__/intersectionObserverMock";

describe("ImageCard", () => {
  const setup = (_props?: Partial<ImageCardProps>) => {
    const props: ImageCardProps = {
      data: {
        server: "test-server",
        id: "test-id",
        secret: "test-secret",
        ownername: "test-ownername",
        title: "test-title",
      },
      ..._props,
    };

    const wrapper = Enzyme.mount(<ImageCard {...props} />);

    return {
      wrapper,
      props,
    };
  };

  const useAddRemoveFavouriteImageMock = jest.spyOn(useImageServices, "useAddRemoveFavouriteImage");

  it("renders without crashing", () => {
    setup();
  });

  it("should show only loader if useAddFavouriteImage return that currently add favourite is loading", () => {
    const handlerMock = () => Promise.resolve();
    useAddRemoveFavouriteImageMock.mockReturnValue({
      isLoading: true,
      isSuccess: false,
      handleAddFavouriteImage: handlerMock,
      handleRemoveFavouriteImage: handlerMock,
      message: "",
    });
    const { wrapper } = setup();

    expect(wrapper.find("[data-testid='add-favourite-loader']").length).toBe(1);
    expect(wrapper.find("[data-testid='add-favourite-success']").length).toBe(0);
    expect(wrapper.find("[data-testid='add-favourite-button']").length).toBe(0);
  });

  it("should show only success message if add favourite is not loading anymore and was successful", () => {
    const handlerMock = () => Promise.resolve();
    useAddRemoveFavouriteImageMock.mockReturnValue({
      isLoading: false,
      isSuccess: true,
      handleAddFavouriteImage: handlerMock,
      handleRemoveFavouriteImage: handlerMock,
      message: "",
    });
    const { wrapper } = setup();

    expect(wrapper.find("[data-testid='add-favourite-loader']").length).toBe(0);
    expect(wrapper.find("[data-testid='add-favourite-success']").length).toBe(1);
    expect(wrapper.find("[data-testid='add-favourite-button']").length).toBe(0);
  });

  it("should show message text returned from useAddRemoveFavouriteImage hook", () => {
    const handlerMock = () => Promise.resolve();
    const message = "test message";
    useAddRemoveFavouriteImageMock.mockReturnValue({
      isLoading: false,
      isSuccess: true,
      handleAddFavouriteImage: handlerMock,
      handleRemoveFavouriteImage: handlerMock,
      message,
    });
    const { wrapper } = setup();

    expect(wrapper.find("[data-testid='add-favourite-success']").text()).toBe(message);
  });

  it("should show only add favourite button if add favourite is not already loading or successful", () => {
    const handlerMock = () => Promise.resolve();
    useAddRemoveFavouriteImageMock.mockReturnValue({
      isLoading: false,
      isSuccess: false,
      handleAddFavouriteImage: handlerMock,
      handleRemoveFavouriteImage: handlerMock,
      message: "",
    });
    const { wrapper } = setup();

    expect(wrapper.find("[data-testid='add-favourite-loader']").length).toBe(0);
    expect(wrapper.find("[data-testid='add-favourite-success']").length).toBe(0);
    expect(wrapper.find("[data-testid='add-favourite-button']").length).toBe(1);
  });
});
