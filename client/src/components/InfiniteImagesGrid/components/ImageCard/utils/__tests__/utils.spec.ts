import getImageClass from "../utils";

describe("utils", () => {
  describe("getImageClass", () => {
    it("should return correct imageClass according to input", () => {
      const testData: Array<[number, string]> = [
        [99999999, "c"],
        [0, "m"],
        [200, "m"],
        [250, "n"],
        [380, "w"],
        [500, "none"],
        [680, "c"],
      ];

      testData.forEach((data) => {
        expect(getImageClass(data[0])).toBe(data[1]);
      });
    });
  });
});
