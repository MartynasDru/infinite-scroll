interface ImageSizes {
  [key: string]: number
}

export const IMAGE_SIZES = <ImageSizes>{
  m: 240,
  n: 320,
  w: 400,
  none: 500,
  z: 640,
  c: 800,
};

export default IMAGE_SIZES;
