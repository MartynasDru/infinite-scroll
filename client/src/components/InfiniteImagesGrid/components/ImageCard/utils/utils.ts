import { IMAGE_SIZES } from "./constants";

const getImageClass = (cardWidth: number): string => {
  const imageSizesEntries = Object.entries(IMAGE_SIZES);
  const firstBiggerSize = imageSizesEntries.find(([, imgSize]) => imgSize >= cardWidth);

  return firstBiggerSize ? firstBiggerSize[0] : imageSizesEntries[imageSizesEntries.length - 1][0];
};

export default getImageClass;
