import React, { ForwardedRef } from "react";
import { joinTruthy } from "utils/utils";
import styles from "./loader.scss";

export interface LoaderProps {
  classNames?: { [key: string]: string };
  isLight?: boolean;
}

export const Loader = React.forwardRef((
  { classNames, isLight }: LoaderProps,
  ref: ForwardedRef<any>,
) => (
  <div
    ref={ref}
    data-testid="loader"
    className={joinTruthy([styles.loader, classNames?.loader])}
  >
    <div
      className={
        joinTruthy(
          [
            styles.loader__spinner,
            isLight && styles["loader__spinner--light"],
            classNames?.spinner,
          ],
        )
      }
    />
  </div>
));
