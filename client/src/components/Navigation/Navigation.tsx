import React, { useState, KeyboardEvent } from "react";
import routes from "app/routes";
import { Link } from "react-router-dom";
import { evaluateIsEnterKeyPressed } from "utils/utils";
import styles from "./navigation.scss";
import { BurgerNavigation } from "./components/BurgerNavigation/BurgerNavigation";

const Navigation = () => {
  const [isDropdownOpen, setIsDropdownOpen] = useState<boolean>(false);
  const navigationRoutes = Object.values(routes).map(({ path, title }) => ({ path, title }));

  const handleToggleOpenKeyDown = (e: KeyboardEvent) => {
    if (evaluateIsEnterKeyPressed(e)) {
      setIsDropdownOpen(!isDropdownOpen);
    }
  };

  return (
    <nav className={styles.navigation}>
      <div className={styles.navigation__tabs}>
        {navigationRoutes.map(({ path, title }) => (
          <Link key={path} to={path}>
            <span data-testid="navigation-tab" className={styles.navigation__tab}>{title}</span>
          </Link>
        ))}
      </div>
      <div
        role="button"
        data-testid="navigation-toggle-open"
        className={styles["navigation__toggle-open"]}
        onClick={() => setIsDropdownOpen(!isDropdownOpen)}
        onKeyPress={handleToggleOpenKeyDown}
        tabIndex={0}
      >
        &#9776;
      </div>
      {isDropdownOpen && (
        <BurgerNavigation
          navigationRoutes={navigationRoutes}
          setIsDropdownOpen={setIsDropdownOpen}
        />
      )}
    </nav>
  );
};

export default Navigation;
