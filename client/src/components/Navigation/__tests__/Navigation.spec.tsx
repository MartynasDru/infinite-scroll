import React from "react";
import Enzyme from "enzyme";
import routes from "app/routes";
import { BrowserRouter } from "react-router-dom";
import Navigation from "../Navigation";

describe("Navigation", () => {
  const setup = () => {
    const wrapper = Enzyme.mount(
      <BrowserRouter>
        <Navigation />
      </BrowserRouter>,
    );

    return {
      wrapper,
    };
  };

  it("renders without crashing", () => {
    setup();
  });

  it("should render as many tabs in navigation as there is in routes configuration", () => {
    const routesCount = Object.values(routes).length;
    const { wrapper } = setup();

    expect(wrapper.find("[data-testid='navigation-tab']").length).toBe(routesCount);
  });

  it("should render as many tabs in navigation as there is in routes configuration", () => {
    const routesCount = Object.values(routes).length;
    const { wrapper } = setup();

    expect(wrapper.find("[data-testid='navigation-tab']").length).toBe(routesCount);
  });

  it("burger navigation should not be rendered by default", () => {
    const { wrapper } = setup();

    expect(wrapper.find("[data-testid='burger-navigation']").length).toBe(0);
  });

  it("should have burger navigation after navigation toggle open click", () => {
    const { wrapper } = setup();
    const navigationToggleOpen = wrapper.find("[data-testid='navigation-toggle-open']");
    navigationToggleOpen.simulate("click");

    expect(wrapper.find("[data-testid='burger-navigation']").length).toBe(1);
  });
});
