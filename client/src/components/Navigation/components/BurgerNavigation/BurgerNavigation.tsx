import React, { Dispatch, SetStateAction } from "react";
import { route } from "app/routes";
import { BurgerNavigationItem } from "./components/BurgerNavigationItem/BurgerNavigationItem";
import styles from "./burger-navigation.scss";

export interface BurgerNavigationProps {
  navigationRoutes: Array<route>;
  setIsDropdownOpen: Dispatch<SetStateAction<boolean>>;
}

export const BurgerNavigation = (
  { navigationRoutes, setIsDropdownOpen }: BurgerNavigationProps,
) => (
  <ul
    data-testid="burger-navigation"
    className={styles["burger-navigation"]}
  >
    {navigationRoutes.map(({ title, path }) => (
      <BurgerNavigationItem
        key={path}
        title={title}
        path={path}
        setIsDropdownOpen={setIsDropdownOpen}
      />
    ))}
  </ul>
);
