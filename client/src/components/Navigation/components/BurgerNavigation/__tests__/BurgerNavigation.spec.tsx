import React from "react";
import Enzyme from "enzyme";
import { BrowserRouter } from "react-router-dom";
import { BurgerNavigation, BurgerNavigationProps } from "../BurgerNavigation";

describe("BurgerNavigation", () => {
  const setup = (_props?: Partial<BurgerNavigationProps>) => {
    const props: BurgerNavigationProps = {
      navigationRoutes: [],
      setIsDropdownOpen: jest.fn(),
      ..._props,
    };

    const wrapper = Enzyme.mount(
      <BrowserRouter>
        <BurgerNavigation {...props} />
      </BrowserRouter>,
    );

    return {
      wrapper,
    };
  };

  it("renders without crashing", () => {
    setup();
  });

  it("should render as many items as there are passed navigation routes", () => {
    const navigationRoutes = [{ title: "", path: "/test-path-1" }, { title: "", path: "/test-path-2" }];
    const { wrapper } = setup({ navigationRoutes });

    expect(wrapper.find("[data-testid='burger-navigation-item']").length).toBe(navigationRoutes.length);
  });
});
