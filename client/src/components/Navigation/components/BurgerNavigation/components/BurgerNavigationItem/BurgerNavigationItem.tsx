import React, { Dispatch, SetStateAction } from "react";
import { Link } from "react-router-dom";
import { evaluateIsEnterKeyPressed } from "utils/utils";
import styles from "./burger-navigation-item.scss";

export interface BurgerNavigationItemProps {
  title: string;
  path: string;
  setIsDropdownOpen: Dispatch<SetStateAction<boolean>>;
}

export const BurgerNavigationItem = (
  { title, path, setIsDropdownOpen }: BurgerNavigationItemProps,
) => {
  const handleNavigationItemKeyDown = (e: React.KeyboardEvent) => {
    if (evaluateIsEnterKeyPressed(e)) {
      setIsDropdownOpen(false);
    }
  };

  return (
    <li
      data-testid="burger-navigation-item"
      className={styles["burger-navigation-item"]}
    >
      <Link
        data-testid="burger-navigation-item-link"
        to={path}
        onClick={() => setIsDropdownOpen(false)}
        onKeyPress={handleNavigationItemKeyDown}
      >
        {title}
      </Link>
    </li>
  );
};
