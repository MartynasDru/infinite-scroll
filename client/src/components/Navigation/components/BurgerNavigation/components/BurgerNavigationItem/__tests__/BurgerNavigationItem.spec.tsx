import React from "react";
import Enzyme from "enzyme";
import { BrowserRouter } from "react-router-dom";
import { BurgerNavigationItem, BurgerNavigationItemProps } from "../BurgerNavigationItem";

describe("BurgerNavigationItem", () => {
  const setup = () => {
    const props: BurgerNavigationItemProps = {
      title: "",
      path: "",
      setIsDropdownOpen: jest.fn(),
    };

    const wrapper = Enzyme.mount(
      <BrowserRouter>
        <BurgerNavigationItem {...props} />
      </BrowserRouter>,
    );

    return {
      wrapper,
      props,
    };
  };

  it("renders without crashing", () => {
    setup();
  });

  it("should call setIsDropdownOpen on click with false argument", () => {
    const { wrapper, props } = setup();
    const item = wrapper.find("[data-testid='burger-navigation-item-link']").hostNodes();
    item.simulate("click");

    expect(props.setIsDropdownOpen).toHaveBeenCalledWith(false);
  });
});
