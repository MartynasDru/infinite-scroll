import "react-app-polyfill/ie11";
import "react-app-polyfill/stable";
import React from "react";
import { BrowserRouter } from "react-router-dom";
import { render } from "react-dom";

import App from "./App";
import "./index.scss";

render(
  <BrowserRouter>
    <App />
  </BrowserRouter>,
  document.getElementById("root"),
);
