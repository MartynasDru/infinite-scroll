import React, { useState } from "react";
import { fetchUserFavouritesList, ImagesData } from "services/useImageServices";
import { InfiniteImagesGrid } from "components/InfiniteImagesGrid/InfiniteImagesGrid";

const Favourites = () => {
  const [favouriteImages, setFavouriteImages] = useState<ImagesData>(null);

  const handleFavouriteImagesLoad = (page: number) => fetchUserFavouritesList(page).then((data) => {
    const updatedImages = favouriteImages ? [...favouriteImages, ...data.photo] : [...data.photo];
    setFavouriteImages(updatedImages);
    return data.pages;
  });

  return (
    <InfiniteImagesGrid
      images={favouriteImages}
      setImages={setFavouriteImages}
      onFetchData={handleFavouriteImagesLoad}
      emptyMessage="Currently you don't have any favourite images"
      isFavouritesTable
    />
  );
};

export default Favourites;
