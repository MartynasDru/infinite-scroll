import React, { useState } from "react";
import { ImagesData, fetchImages } from "services/useImageServices";
import { InfiniteImagesGrid } from "components/InfiniteImagesGrid/InfiniteImagesGrid";

const RecentImages = () => {
  const [images, setImages] = useState<ImagesData>(null);

  const handleImagesLoad = (page: number) => fetchImages(page).then((data) => {
    const updatedImages = images ? [...images, ...data.photo] : [...data.photo];
    setImages(updatedImages);
    return data.pages;
  });

  return <InfiniteImagesGrid images={images} onFetchData={handleImagesLoad} />;
};

export default RecentImages;
