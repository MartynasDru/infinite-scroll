import windowLocationMock from "__mocks__/windowLocationMock";
import * as flickrAuthenticationService from "../flickrAuthenticationService";

const {
  getOAuthToken, getOAuthAccessToken, getFlickrData, authenticateUser,
} = flickrAuthenticationService;

describe("flickrAuthenticationService", () => {
  beforeEach(() => {
    localStorage.clear();
    window.URLSearchParams = jest.fn().mockReturnValue({ get: () => {} });
  });

  describe("getOAuthToken", () => {
    it("should set flickr_oauth_token, flickr_oauth_token_secret to keys in localStorage with values that are passed from fetch and redirect user to flickr url", () => {
      const oauthToken = "test-token";
      const oauthTokenSecret = "test-token-secret";
      window.fetch = jest.fn()
        .mockReturnValue(
          Promise.resolve({ json: () => ({ oauthToken, oauthTokenSecret }) }),
        );
      const locationReplaceMock = windowLocationMock();

      getOAuthToken().then(() => {
        expect(localStorage.getItem("flickr_oauth_token")).toBe(oauthToken);
        expect(localStorage.getItem("flickr_oauth_token_secret")).toBe(oauthTokenSecret);
        expect(locationReplaceMock).toHaveBeenCalledWith(`https://www.flickr.com/services/oauth/authorize?oauth_token=${oauthToken}&perms=write`);
      });
    });
  });

  describe("getOAuthAccessToken", () => {
    it("should set user_nsid to returned one from /access_token endpoint key in localStorage after successful authorization", () => {
      const userNsid = "test-user-nsid";
      window.fetch = jest.fn().mockReturnValue(Promise.resolve({ json: () => ({ userNsid }) }));

      getOAuthAccessToken().then(() => {
        expect(localStorage.getItem("flickr_user_nsid")).toBe(userNsid);
      });
    });
  });

  describe("getFlickrData", () => {
    it("should take flickr user authentication data from local storage and return them", () => {
      localStorage.setItem("flickr_oauth_token", "value-1");
      localStorage.setItem("flickr_oauth_token_secret", "value-2");
      localStorage.setItem("flickr_oauth_verifier", "value-3");
      localStorage.setItem("flickr_user_nsid", "value-4");

      expect(getFlickrData()).toEqual({
        oauthToken: "value-1",
        oauthTokenSecret: "value-2",
        oauthVerifier: "value-3",
        userNsid: "value-4",
      });
    });
  });

  describe("authenticateUser", () => {
    it("should set flickr_oauth_verifier key in localStorage if it is found by URLSearchParams", () => {
      const oauthVerifier = "test-oauth-verifier";
      window.fetch = jest.fn().mockReturnValue(Promise.resolve({ json: () => ({}) }));
      window.URLSearchParams = jest.fn().mockReturnValue({ get: () => oauthVerifier });
      authenticateUser();

      expect(localStorage.getItem("flickr_oauth_verifier")).toBe(oauthVerifier);
    });

    it("should call getOAuthToken if there is no oauth_token or oauth_token_secret currently set", () => {
      const getOAuthTokenMock = jest.fn();
      jest
        .spyOn(flickrAuthenticationService, "getOAuthToken")
        .mockImplementation(getOAuthTokenMock);
      authenticateUser();

      expect(getOAuthTokenMock).toHaveBeenCalledTimes(1);
    });

    it("should call getOAuthAccessToken if there is no user_nsid currently set, but user is already authenticated and has oauth_token, oauth_token_secret", () => {
      localStorage.setItem("flickr_oauth_token", "test-oauth-token");
      localStorage.setItem("flickr_oauth_token_secret", "test-oauth-token-secret");
      window.fetch = jest.fn().mockReturnValue(Promise.resolve({ json: () => ({}) }));
      const locationReplaceMock = windowLocationMock();
      const getOAuthAccessTokenMock = jest.fn();
      jest
        .spyOn(flickrAuthenticationService, "getOAuthAccessToken")
        .mockImplementation(getOAuthAccessToken);
      authenticateUser().then(() => {
        expect(getOAuthAccessTokenMock).toHaveBeenCalledTimes(1);
        expect(locationReplaceMock).toHaveBeenCalledWith("https://www.flickr.com/services/oauth/authorize?oauth_token=test-oauth-token&perms=write");
      });
    });
  });
});
