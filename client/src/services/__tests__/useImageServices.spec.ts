import { fetchImages, fetchUserFavouritesList } from "../useImageServices";

describe("useImageServices", () => {
  describe("fetchImages", () => {
    it("should fetch and return photos from returned data", () => {
      const data = {
        photos: ["photo-1", "photo-2"],
      };
      window.fetch = jest.fn().mockReturnValue(Promise.resolve({ json: () => data }));

      fetchImages(1).then((photos) => {
        expect(photos).toEqual(data.photos);
      });
    });
  });

  describe("fetchUserFavouritesList", () => {
    it("should not do anything if user does not have user_nsid set", () => {
      expect(fetchUserFavouritesList(1)).toBe(undefined);
    });

    it("should fetch and return photos from returned data", () => {
      const userNsid = "test-user-nsid";
      localStorage.setItem("flickr_user_nsid", userNsid);
      const data = {
        photos: ["photo-1", "photo-2"],
      };
      window.fetch = jest.fn().mockReturnValue(Promise.resolve({ json: () => data }));

      fetchUserFavouritesList(1).then((photos) => {
        expect(photos).toEqual(data.photos);
      });
    });
  });
});
