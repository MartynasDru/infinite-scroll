import { objectToParams } from "../utils/utils";

export const getOAuthToken = () => fetch(`${process.env.REACT_APP_API_URL}/request_token`)
  .then((res) => res.json()).then(({ oauthToken, oauthTokenSecret }) => {
    localStorage.setItem("flickr_oauth_token", oauthToken);
    localStorage.setItem("flickr_oauth_token_secret", oauthTokenSecret);

    window.location.replace(`https://www.flickr.com/services/oauth/authorize?oauth_token=${oauthToken}&perms=write`);
  });

export const getFlickrData = () => {
  const oauthToken = localStorage.getItem("flickr_oauth_token");
  const oauthTokenSecret = localStorage.getItem("flickr_oauth_token_secret");
  const oauthVerifier = localStorage.getItem("flickr_oauth_verifier");
  const userNsid = localStorage.getItem("flickr_user_nsid");

  return {
    oauthToken,
    oauthTokenSecret,
    oauthVerifier,
    userNsid,
  };
};

export const getOAuthAccessToken = () => {
  const { oauthToken, oauthTokenSecret, oauthVerifier } = getFlickrData();

  const params = objectToParams({
    oauthToken,
    oauthTokenSecret,
    oauthVerifier,
  });

  return fetch(`${process.env.REACT_APP_API_URL}/access_token?${params}`)
    .then((res) => res.json())
    .then(({ userNsid }) => {
      if (userNsid) {
        localStorage.setItem("flickr_user_nsid", userNsid);
      } else {
        getOAuthToken();
      }
    });
};

export const authenticateUser = (search?: string) => {
  const { oauthToken, oauthTokenSecret, oauthVerifier } = getFlickrData();
  const searchForUrlParams = new URLSearchParams(search);
  const oauthVerifierParam = searchForUrlParams.get("oauth_verifier");

  if (oauthVerifierParam) {
    localStorage.setItem("flickr_oauth_verifier", oauthVerifierParam);
  }

  if (!oauthToken || !oauthTokenSecret) {
    return getOAuthToken();
  }

  if (!oauthVerifierParam && !oauthVerifier) {
    window.location.replace(`https://www.flickr.com/services/oauth/authorize?oauth_token=${oauthToken}&perms=write`);
  }

  return getOAuthAccessToken();
};
