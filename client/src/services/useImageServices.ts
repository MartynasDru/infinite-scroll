import { useState } from "react";
import { objectToParams } from "../utils/utils";
import { getFlickrData } from "./flickrAuthenticationService";

export interface ImageData {
  server: string;
  id: string;
  secret: string;
  ownername: string;
  title: string;
}

export type ImagesData = Array<ImageData>;

export const fetchImages = (page: number) => {
  const userId = "82831944@N02"; // Using this user's photo gallery for testing/development, because public photos are not the best
  const params = objectToParams({
    method: "flickr.people.getPublicPhotos",
    user_id: userId,
    api_key: process.env.REACT_APP_FLICKR_API_KEY,
    extras: "owner_name",
    per_page: 30,
    page,
    format: "json",
    nojsoncallback: 1,
  });

  return fetch(
    `https://api.flickr.com/services/rest/?${params}`,
  ).then((res) => res.json()).then((data) => data.photos);
};

interface useAddRemoveFavouriteImageReturn {
  isLoading: boolean;
  isSuccess: boolean;
  handleAddFavouriteImage: (id: string) => Promise<void>;
  handleRemoveFavouriteImage: (id: string) => Promise<void>;
  message: string;
}

export const useAddRemoveFavouriteImage = (): useAddRemoveFavouriteImageReturn => {
  const [isLoading, setIsLoading] = useState(false);
  const [isSuccess, setIsSuccess] = useState(false);
  const [message, setMessage] = useState("");

  const handleAddFavouriteImage = (id: string) => {
    setIsLoading(true);
    const data = getFlickrData();
    const params = objectToParams({
      photoId: id,
      ...data,
    });

    return fetch(`${process.env.REACT_APP_API_URL}/add_favourite_image?${params}`)
      .then((res) => res.json())
      .then((resData) => {
        setMessage(resData.message);
        setIsLoading(false);
        setIsSuccess(true);
      });
  };

  const handleRemoveFavouriteImage = (id: string) => {
    setIsLoading(true);
    const data = getFlickrData();
    const params = objectToParams({
      photoId: id,
      ...data,
    });

    return fetch(
      `${process.env.REACT_APP_API_URL}/remove_favourite_image?${params}`,
    ).then(() => {
      setIsLoading(false);
    });
  };

  return {
    isLoading,
    isSuccess,
    handleAddFavouriteImage,
    handleRemoveFavouriteImage,
    message,
  };
};

export const fetchUserFavouritesList = (page: number) => {
  const { userNsid } = getFlickrData();

  if (userNsid) {
    const params = objectToParams({
      method: "flickr.favorites.getList",
      api_key: process.env.REACT_APP_FLICKR_API_KEY,
      user_id: userNsid,
      extras: "owner_name",
      per_page: 30,
      page,
      format: "json",
      nojsoncallback: 1,
    });

    return fetch(
      `https://api.flickr.com/services/rest/?${params}`,
    ).then((res) => res.json()).then((data) => data.photos);
  }
};
