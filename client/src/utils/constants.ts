const KEYS = {
  enter: {
    code: "Enter",
    keyCode: 13,
  },
};

export default KEYS;
