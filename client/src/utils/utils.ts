import { KeyboardEvent } from "react";
import KEYS from "./constants";

export const joinTruthy = (items: Array<string | number | boolean | null | undefined>, delimiter = " ") => (items || []).filter(Boolean).join(delimiter);

export const objectToParams = (object: object) => {
  const params = Object.entries(object).map((param) => param.join("="));

  return params.join("&");
};

export const evaluateIsEnterKeyPressed = (e: KeyboardEvent) => KEYS.enter.code === e.key;
